module gitlab.com/gomidi/midi/tools/smfsysex

go 1.19

require (
	github.com/emersion/go-appdir v1.1.2 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	gitlab.com/gomidi/midi/v2 v2.0.25 // indirect
	gitlab.com/metakeule/config v1.21.2 // indirect
	gitlab.com/metakeule/fmtdate v1.2.0 // indirect
	gitlab.com/metakeule/version v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20201211090839-8ad439b19e0f // indirect
)
